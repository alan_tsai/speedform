USE [BPM]
GO
/****** Object:  Table [dbo].[Newtype_ConfigCenter_ScopeList]    Script Date: 2020/8/20 下午 07:54:56 ******/
DROP TABLE [dbo].[Newtype_ConfigCenter_ScopeList]
GO
/****** Object:  Table [dbo].[Newtype_ConfigCenter_LangValue]    Script Date: 2020/8/20 下午 07:54:56 ******/
DROP TABLE [dbo].[Newtype_ConfigCenter_LangValue]
GO
/****** Object:  Table [dbo].[Newtype_ConfigCenter_ConfigScope]    Script Date: 2020/8/20 下午 07:54:56 ******/
DROP TABLE [dbo].[Newtype_ConfigCenter_ConfigScope]
GO
/****** Object:  Table [dbo].[Newtype_ConfigCenter_ConfigMemberSortNo]    Script Date: 2020/8/20 下午 07:54:56 ******/
DROP TABLE [dbo].[Newtype_ConfigCenter_ConfigMemberSortNo]
GO
/****** Object:  Table [dbo].[Newtype_ConfigCenter_ConfigMemberScope]    Script Date: 2020/8/20 下午 07:54:56 ******/
DROP TABLE [dbo].[Newtype_ConfigCenter_ConfigMemberScope]
GO
/****** Object:  Table [dbo].[Newtype_ConfigCenter_ConfigMemberDetail]    Script Date: 2020/8/20 下午 07:54:56 ******/
DROP TABLE [dbo].[Newtype_ConfigCenter_ConfigMemberDetail]
GO
/****** Object:  Table [dbo].[Newtype_ConfigCenter_ConfigMember]    Script Date: 2020/8/20 下午 07:54:56 ******/
DROP TABLE [dbo].[Newtype_ConfigCenter_ConfigMember]
GO
/****** Object:  Table [dbo].[Newtype_ConfigCenter_ConfigList]    Script Date: 2020/8/20 下午 07:54:56 ******/
DROP TABLE [dbo].[Newtype_ConfigCenter_ConfigList]
GO
/****** Object:  Table [dbo].[Newtype_ConfigCenter_ConfigField]    Script Date: 2020/8/20 下午 07:54:56 ******/
DROP TABLE [dbo].[Newtype_ConfigCenter_ConfigField]
GO
/****** Object:  Table [dbo].[Newtype_ConfigCenter_ConfigAuth]    Script Date: 2020/8/20 下午 07:54:56 ******/
DROP TABLE [dbo].[Newtype_ConfigCenter_ConfigAuth]
GO
/****** Object:  Table [dbo].[Newtype_ConfigCenter_AuthList]    Script Date: 2020/8/20 下午 07:54:56 ******/
DROP TABLE [dbo].[Newtype_ConfigCenter_AuthList]
GO
/****** Object:  Table [dbo].[Newtype_ConfigCenter_AuthList]    Script Date: 2020/8/20 下午 07:54:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Newtype_ConfigCenter_AuthList](
	[AuthID] [uniqueidentifier] NOT NULL,
	[AuthType] [nvarchar](50) NULL,
 CONSTRAINT [PK_Newtype_ConfigCenter_AuthList] PRIMARY KEY CLUSTERED 
(
	[AuthID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Newtype_ConfigCenter_ConfigAuth]    Script Date: 2020/8/20 下午 07:54:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Newtype_ConfigCenter_ConfigAuth](
	[ConfigID] [uniqueidentifier] NOT NULL,
	[AuthID] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_Newtype_ConfigCenter_ConfigAuth] PRIMARY KEY CLUSTERED 
(
	[ConfigID] ASC,
	[AuthID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Newtype_ConfigCenter_ConfigField]    Script Date: 2020/8/20 下午 07:54:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Newtype_ConfigCenter_ConfigField](
	[ConfigFieldID] [uniqueidentifier] NOT NULL,
	[ConfigID] [uniqueidentifier] NOT NULL,
	[FieldName] [nvarchar](50) NULL,
	[SortNo] [int] NULL,
	[Enable] [int] NULL,
	[DetailType] [int] NULL,
	[OptionMode] [nvarchar](50) NULL,
	[ReadOnly] [int] NULL,
	[Multilingualism] [int] NULL,
	[UniqueMode] [int] NULL,
 CONSTRAINT [PK_Newtype_ConfigCenter_ConfigField_1] PRIMARY KEY CLUSTERED 
(
	[ConfigFieldID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Newtype_ConfigCenter_ConfigList]    Script Date: 2020/8/20 下午 07:54:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Newtype_ConfigCenter_ConfigList](
	[ConfigID] [uniqueidentifier] NOT NULL,
	[ConfigName] [nvarchar](50) NULL,
	[Identify] [nvarchar](50) NULL,
	[Insert] [int] NULL,
	[Delete] [int] NULL,
	[Enable] [int] NULL,
 CONSTRAINT [PK_Newtype_Global_ConfigList] PRIMARY KEY CLUSTERED 
(
	[ConfigID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Newtype_ConfigCenter_ConfigMember]    Script Date: 2020/8/20 下午 07:54:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Newtype_ConfigCenter_ConfigMember](
	[ConfigFieldID] [uniqueidentifier] NOT NULL,
	[ConfigMemberID] [uniqueidentifier] NOT NULL,
	[ConfigID] [uniqueidentifier] NULL,
	[Value] [nvarchar](max) NULL,
	[Enable] [int] NULL,
 CONSTRAINT [PK_Newtype_ConfigCenter_ConfigMember] PRIMARY KEY CLUSTERED 
(
	[ConfigFieldID] ASC,
	[ConfigMemberID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Newtype_ConfigCenter_ConfigMemberDetail]    Script Date: 2020/8/20 下午 07:54:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Newtype_ConfigCenter_ConfigMemberDetail](
	[UniqueID] [int] IDENTITY(1,1) NOT NULL,
	[ConfigMemberID] [uniqueidentifier] NULL,
	[ConfigFieldID] [uniqueidentifier] NOT NULL,
	[Msg] [nvarchar](max) NULL,
	[Value] [nvarchar](max) NULL,
 CONSTRAINT [PK_Newtype_ConfigCenter_ConfigMemberDetail] PRIMARY KEY CLUSTERED 
(
	[UniqueID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Newtype_ConfigCenter_ConfigMemberScope]    Script Date: 2020/8/20 下午 07:54:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Newtype_ConfigCenter_ConfigMemberScope](
	[ScopeID] [uniqueidentifier] NOT NULL,
	[ConfigMemberID] [uniqueidentifier] NOT NULL,
	[Value] [nvarchar](50) NULL,
 CONSTRAINT [PK_Newtype_ConfigCenter_AuthMember] PRIMARY KEY CLUSTERED 
(
	[ScopeID] ASC,
	[ConfigMemberID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Newtype_ConfigCenter_ConfigMemberSortNo]    Script Date: 2020/8/20 下午 07:54:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Newtype_ConfigCenter_ConfigMemberSortNo](
	[ConfigMemberID] [uniqueidentifier] NOT NULL,
	[ConfigID] [uniqueidentifier] NULL,
	[SortNo] [int] NULL,
 CONSTRAINT [PK_Newtype_ConfigCenter_ConfigMemberSort] PRIMARY KEY CLUSTERED 
(
	[ConfigMemberID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Newtype_ConfigCenter_ConfigScope]    Script Date: 2020/8/20 下午 07:54:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Newtype_ConfigCenter_ConfigScope](
	[ConfigID] [uniqueidentifier] NOT NULL,
	[ScopeID] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_Newtype_ConfigCenter_ConfigScope] PRIMARY KEY CLUSTERED 
(
	[ConfigID] ASC,
	[ScopeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Newtype_ConfigCenter_LangValue]    Script Date: 2020/8/20 下午 07:54:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Newtype_ConfigCenter_LangValue](
	[ConfigMemberID] [uniqueidentifier] NOT NULL,
	[ConfigFieldID] [uniqueidentifier] NOT NULL,
	[Lang] [nvarchar](10) NOT NULL,
	[Value] [nvarchar](max) NULL,
 CONSTRAINT [PK_Newtype_ConfigCenter_LangValue] PRIMARY KEY CLUSTERED 
(
	[ConfigMemberID] ASC,
	[ConfigFieldID] ASC,
	[Lang] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Newtype_ConfigCenter_ScopeList]    Script Date: 2020/8/20 下午 07:54:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Newtype_ConfigCenter_ScopeList](
	[ScopeID] [uniqueidentifier] NOT NULL,
	[ScopeType] [nvarchar](50) NULL,
 CONSTRAINT [PK_Newtype_ConfigCenter_AuthScope] PRIMARY KEY CLUSTERED 
(
	[ScopeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


/****** Object:  View [dbo].[Newtype_ConfigCenterView_DiagramFolder]    Script Date: 2020/8/20 下午 08:45:24 ******/
DROP VIEW [dbo].[Newtype_ConfigCenterView_DiagramFolder]
GO

/****** Object:  View [dbo].[Newtype_ConfigCenterView_DiagramFolder]    Script Date: 2020/8/20 下午 08:45:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[Newtype_ConfigCenterView_DiagramFolder]
AS
SELECT         dbo.FSe7en_Sys_DiagramFolder.FolderGuid, 
                          dbo.NTFM7_GetLang_DiagramFolder(dbo.FSe7en_Sys_DiagramFolder.FolderGuid,
                           dbo.FSe7en_Tep_LanguageSetting.LanguageID) AS FolderName, 
                          dbo.FSe7en_Sys_DiagramFolder.SortOrder, 
                          dbo.FSe7en_Sys_DiagramFolder.ExpandFlag, 
                          dbo.FSe7en_Sys_DiagramFolder.ParentFolder, 
                          dbo.FSe7en_Tep_LanguageSetting.LanguageID AS Lang
FROM             dbo.FSe7en_Sys_DiagramFolder WITH (NOLOCK) CROSS JOIN
                          dbo.FSe7en_Tep_LanguageSetting WITH (NOLOCK)
WHERE         (dbo.FSe7en_Tep_LanguageSetting.EnableFlag = 1)
UNION
SELECT         '0', '通用', 1, 1, '', 'zh-tw'


GO

/****** Object:  View [dbo].[Newtype_ConfigCenterView_DiagramList]    Script Date: 2020/8/20 下午 08:45:31 ******/
DROP VIEW [dbo].[Newtype_ConfigCenterView_DiagramList]
GO

/****** Object:  View [dbo].[Newtype_ConfigCenterView_DiagramList]    Script Date: 2020/8/20 下午 08:45:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[Newtype_ConfigCenterView_DiagramList]
AS
SELECT         IdentifyList.Identify, dbo.NTFM7_GetLang_DiagramList(IdentifyList.Identify, 
                          dbo.FSe7en_Tep_LanguageSetting.LanguageID) AS DisplayName, 
                          IdentifyList.FolderGuid, 
                          dbo.NTFM7_GetLang_DiagramFolder(IdentifyList.FolderGuid, 
                          dbo.FSe7en_Tep_LanguageSetting.LanguageID) AS FolderName, 
                          IdentifyList.Component, ISNULL(dbo.FSe7en_Tep_IdentifySetting.FillerType, 1) 
                          AS FillerType, ISNULL(dbo.FSe7en_Tep_IdentifySetting.EmailSignType, 0) 
                          AS EmailSignType, ISNULL(dbo.FSe7en_Tep_IdentifySetting.MobileSignType, 0)
                           AS MobileSignType, dbo.FSe7en_Tep_IdentifySetting.HelpContent, 
                          ISNULL(dbo.FSe7en_Tep_IdentifySetting.ApplicantType, 1) AS ApplicantType, 
                          dbo.FSe7en_Tep_IdentifySetting.SortID, 
                          dbo.FSe7en_Tep_LanguageSetting.LanguageID AS Lang
FROM             (SELECT DISTINCT Identify, Component, FolderGuid
                           FROM              dbo.FSe7en_Sys_DiagramList WITH (NOLOCK)
                           WHERE          (MTable <> '#HOST!')) AS IdentifyList LEFT OUTER JOIN
                          dbo.FSe7en_Tep_IdentifySetting WITH (NOLOCK) ON 
                          IdentifyList.Identify = dbo.FSe7en_Tep_IdentifySetting.Identify CROSS JOIN
                          dbo.FSe7en_Tep_LanguageSetting WITH (NOLOCK)
WHERE         (dbo.FSe7en_Tep_LanguageSetting.EnableFlag = 1)
UNION
SELECT         '', '通用設定', '0', '通用', '', 1, 0, 0, NULL, 1, 0, 'zh-tw'


GO

/****** Object:  UserDefinedFunction [dbo].[NewType_ConfigCenter_GetValue]    Script Date: 2020/8/20 下午 08:46:25 ******/
DROP FUNCTION [dbo].[NewType_ConfigCenter_GetValue]
GO

/****** Object:  UserDefinedFunction [dbo].[NewType_ConfigCenter_GetValue]    Script Date: 2020/8/20 下午 08:46:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[NewType_ConfigCenter_GetValue] (@ConfigMemberID uniqueidentifier, @ConfigFieldID uniqueidentifier, @Lang nvarchar(10))
RETURNS nvarchar(max)
AS
BEGIN
DECLARE
@Value nvarchar(max)

SET @Value = ISNULL((SELECT TOP (1) Value
					 FROM	Newtype_ConfigCenter_LangValue
					 WHERE  (ConfigMemberID = @ConfigMemberID) AND 
							(ConfigFieldID = @ConfigFieldID) AND 
							(Lang = @Lang)),
					(SELECT TOP (1) Value 
					 FROM   Newtype_ConfigCenter_ConfigMember
					 WHERE	(ConfigMemberID = @ConfigMemberID) AND 
							(ConfigFieldID = @ConfigFieldID)))

RETURN @Value
END


GO
