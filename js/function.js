/**
 * 建立GUID字串回傳
 * @return {string} GUID
 */
function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

function getAttrValue(value) {
    if (typeof value == 'undefined') return null;
    return value;
}

function isUseProcess(gotProcess, process) {
    var process = getAttrValue(process);
    if (process == null) return true;

    var isForward = (process.indexOf('!') < 0);
    var isGotProcess = (process.indexOf(gotProcess) >= 0);
    return (isForward ? isGotProcess : !isGotProcess);
}