$f.prototype.detail = {   //明細模組
    event: {
        AddEventFunction: function(detailId, detailItem){},   //新增明細後Function
        handlerAdd: function(func){this.AddEventFunction = func;},   //新增明細後Function
    },
    template:{},
    setTemplate: function(id, selector){
        $(selector)
            .attr('detailId', id)
            .attr('detailType', 'template')
            .hide();
        return id;
    },
    add: function(id){ //新增明細
        var item = 
        $('[detailId="'+id+'"][detailtype=template]')
            .clone()
            .attr('detailType', 'item')
            .attr('detailItemId', id)
            .show();

        $('[detailId="'+id+'"][detailtype=template]').parent().append(item);
        
        this.event.AddEventFunction(id, item);
        return item;
    },
    delete: function(itemChild){ //刪除明細
        var id = this.getId(itemChild);
        $('[detailItemId='+id+']').remove();
    },
    getId: function(itemChild){ //取得明細ID
        return this.getItem(itemChild).attr('detailItemId');
    },
    getItem: function(itemChild){ //取得明細
        return $(itemChild).parents('[detailtype=item]:first');
    }
};