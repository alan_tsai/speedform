var gulp       = require('gulp'),
    concat     = require('gulp-concat'),
    minifyCSS  = require('gulp-minify-css'),
    uglify     = require('gulp-uglify'),
    rename     = require("gulp-rename");

gulp.task('default', function() {
   return gulp
    .src(['./js/function.js','./js/extends.js','./js/index.js','./js/view.js','./js/detail.js','./js/api.js','./js/dialog.js','./js/verify.js','./js/data.js'])
    .pipe(concat('speedForm.js'))
    .pipe(gulp.dest('./build/'));
})