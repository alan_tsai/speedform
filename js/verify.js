/*
	小數點檢查
	function CheckNumberValue(obj){
		var value = $(obj).val().replace(/[^\d.]/g,'');
		value = (value == '' ? 0 : Math.round(value*100)/100);
		if(value > 999.99) value = 999.99;
		$(obj).val(value);
	}
*/
$f.prototype.verify = {
    temp: {
        requiredElement: {}
    },
    message: {
        required: '{field} 不可空白'
    },
    checkFunction: {
        base: function (verify, elements) {
            var obj = this;
            var status = true;
            $(elements).each(function (i, element) {
                if (obj[verify](element)) {
                    status = false;
                    return;
                }
            });
            return status;
        },
        required: function (element) {
            var name = $(element).attr('name');

            if(typeof this.parent.temp.requiredElement[name] != 'undefined') return false;

            this.parent.temp.requiredElement[name] = element;

            if ($(element).is('[type=checkbox],[type=radio]')) {
                return $('[name=' + name + ']:checked').length == 0;
            } else {
                return $(element).val() == '';
            }
        }
    },
    event: {
        getFieldNameEventFunction: function (element, fieldName) {
            return fieldName;
        },
        handlerGetFieldName: function (func) {
            this.getFieldNameEventFunction = func;
            return this.parent;
        },
    },
    bind: function () {
        var obj = this;
        $('[sf-verify]').each(function () {
            var verify = $(this).attr('sf-verify');

            switch (verify) {
                case 'required':
                    break;
            }
        });
    },
    check: function (isAlert) {
        this.temp.requiredElement = {};

        if (typeof isAlert == 'undefined') isAlert = true;

        var obj = this;
        var verifyMessageArry = [];
        $('[sf-verify]').each(function () {
            if (!isUseProcess(obj.parent.info.processID, $(this).attr('sf-verify-process'))) return;

            var elements = [this];
            var verify = $(this).attr('sf-verify');
            var fieldName = $(this).attr('sf-verify-fieldName');
            var group = $(this).attr('sf-verify-group');

            if (typeof group != 'undefined') {
                var groupElements = $(group);
                if (groupElements.length > 0) elements = elements.concat(groupElements);
            }

            if (!obj.checkFunction.base(verify, elements))
                verifyMessageArry.push(obj.message[verify].replace('{field}', typeof fieldName == 'undefined' ? obj.getFieldName(this) : fieldName));
        });
        if (isAlert && verifyMessageArry.length > 0) alert(verifyMessageArry.join('\n'));

        return {
            verifyMessageArry: verifyMessageArry,
            status: verifyMessageArry.length == 0
        }
    },
    getFieldName: function (element) {
        var fieldName = $(element).attr('sf-verify-fieldName');

        if (typeof fieldName == 'undefined') fieldName = $(element).parents('td:first').prev('th').text().replace('*', '').trim();
        return this.event.getFieldNameEventFunction(element, fieldName);
    },
};