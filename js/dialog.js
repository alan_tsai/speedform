$f.prototype.dialog = {
    bind: function (options) {
        var isIE = function () {
            return ("ActiveXObject" in window);
        }

        var defaults = {
            title: '請選擇', //標題-
            src: '', //網址-
            width: 800, //寬度-
            height: 600, //高度-
            multiple: 0, //0:單選,1:多選 -
            load: function (o) {},
            closeButtonEvent: function (o) {
                $('input:eq(0)').focus();
                $('input:eq(1)').focus();
            },
            buttons: //按鈕- 
                [
                    /*{
                    	text: 'OK',
                    	callback: function(o){
                    		alert($(o).contents().find('[f=y]').val());
                    	}
                    }*/
                ],
            objevent: //Dialog內物件的事件 
                [
                    /*{
                    	tag: 'iframe',
                    	event: 'load',
                    	callback: function(o){
                    		$(o).contents().find('#ifmbkSearch').contents().delegate('a', 'click', function(){
                    			alert('User clicked on foo.');
                    		});
                    	}
                    }*/
                ]
        };
        var options = $.extend(defaults, options);
        var objDialog = {
            dialog: null,
            frame: null,
            content: null,
            open: null
        };
        var buttons = [];
        var no = guid();
        var divDialog = '<div name="divDialog_' + no + '" style="display:none;overflow:hidden;padding:4px;">' +
            '	<div><iframe name="frameDialog_' + no + '" frameborder="0" ' + (options.multiple == 0 ? '' : 'style="float: left; width: 70%;"') + ' width="100%" height="' + (options.buttons.length > 0 ? options.height - 125 : options.height - 50) + '"></iframe></div>' +
            '   ' + (options.multiple == 0 ? '' : '	<div><select name="frameDialogSelect_' + no + '" multiple="multiple" style="font-size: 12px;width: 30%; height: 100%;"></div>') +
            '</div>';

        if (isIE()) options.width += 15;

        objDialog.dialog = $(divDialog).dialog({
            position: {
                my: "center bottom",
                at: "center top",
                of: window
            },
            title: options.title,
            autoOpen: false,
            height: options.height + 4,
            width: options.width,
            modal: true,
            close: function () {
                $('[name=frameDialog_' + no + ']').unbind('load').remove();
                $('[name=divDialog_' + no + ']').unbind('load').remove();
            },
            create: function () {
                $(this)
                    .closest('div.ui-dialog')
                    .find('.ui-dialog-titlebar-close')
                    .click(function (e) {
                        options.closeButtonEvent(objDialog);
                    });
            }
        });

        objDialog.getMultipleSelect = function () {
            return $('[name=frameDialogSelect_' + no + ']');
        }
        objDialog.open = function () {
            objDialog.dialog.dialog('open');
        }
        objDialog.close = function () {
            objDialog.dialog.dialog('close');
        }
        objDialog.frame = $('[name=frameDialog_' + no + ']')[0];

        $(objDialog.frame).load(function () {
            //Add Button
            buttons = [];
            $(options.buttons).each(function (i, o) {
                buttons.push({
                    text: o.text,
                    click: function () {
                        o.callback(objDialog)
                    }
                });
            });
            objDialog.dialog.dialog({
                buttons: buttons
            });

            objDialog.content = $('[name=frameDialog_' + no + ']').contents();
            objDialog.contentWindow = $('[name=frameDialog_' + no + ']')[0].contentWindow ? $('[name=frameDialog_' + no + ']')[0].contentWindow : $('[name=frameDialog_' + no + ']')[0].contentDocument.defaultView;
            options.load(objDialog);

            var frameDialog = this;

            /*
            $(frameDialog).css('height',0).css('height',$(frameDialog).contents().find('body')[0].scrollHeight);
            */

            //Add Window Object Event
            $(options.objevent).each(function (i, o) {
                if (o.event == 'load') {
                    var eventSet = 0;
                    $(frameDialog).contents().find(o.tag).bind(o.event, function () {
                        eventSet = 1;
                        o.callback(objDialog, this);
                    });
                    if ($(frameDialog).contents().find(o.tag).contents().find('body').length > 0 && eventSet == 0) {
                        o.callback(objDialog, this);
                    }
                }
                if (o.event == 'blur') {
                    $(frameDialog).contents().find(o.tag).bind(o.event, function () {
                        o.callback(objDialog, this);
                    });
                } else {
                    $(frameDialog).contents().delegate(o.tag, o.event, function () {
                        o.callback(objDialog, this);
                    });
                }
            });
        }).attr('src', options.src + (options.src.toString().indexOf('?') < 0 ? '?' : '&') + 'r=' + Math.random());
        return objDialog;
    },
    selector: function (options) {
        var defaults = {
            self: null,
            selectorID: '',
            filterSelectorID: '',
            accountID: '',
            defaultFilterString: '',
            multiple: 0, //0:單選,1:多選 -
            multipleFormate: '"data[fieldName]"',
            dataSource: 'db', //db, url, data arry -
            secondDataSource: null, //db, url, data arry, function(FilterStr){} -
            url: '',
            title: '請選擇',
            width: 800,
            height: 600,
            callback: function (selector, data) {}
        };
        var options = $.extend(defaults, options);

        var buttons = [];

        if (options.multiple == 1)
            buttons.push({
                text: 'Delete',
                callback: function (selector) {
                    selector.contentWindow.deleteOption();
                }
            });

        buttons.push({
            text: 'Ok',
            callback: function (selector) {
                var data = selector.contentWindow.getData();
                options.callback(selector, data);
            }
        });

        buttons.push({
            text: 'Cancel',
            callback: function (selector) {
                selector.close();
            }
        });
        var dialog = this.bind({
            load: function (selector) {
                selector.contentWindow.init(options, selector);
                selector.contentWindow.loadData(options.defaultFilterString, '');
            },
            height: options.height,
            width: options.width,
            title: options.title,
            multiple: options.multiple,
            src: 'Newtype_SFSelector.aspx?SelectorID=' + options.selectorID + '&FilterSelectorID=' + options.filterSelectorID + '&AccountID=' + options.accountID + '&Multiple=' + options.multiple + '&url=' + encodeURIComponent(options.url) + '&r=' + Math.random(),
            buttons: buttons
        });
        dialog.open();
    }
};