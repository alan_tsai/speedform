/**
 * 建立GUID字串回傳
 * @return {string} GUID
 */
function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

function getAttrValue(value) {
    if (typeof value == 'undefined') return null;
    return value;
}

function isUseProcess(gotProcess, process) {
    var process = getAttrValue(process);
    if (process == null) return true;

    var isForward = (process.indexOf('!') < 0);
    var isGotProcess = (process.indexOf(gotProcess) >= 0);
    return (isForward ? isGotProcess : !isGotProcess);
}
// (new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423 
// (new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18 
Date.prototype.Format = function (fmt) { //author: meizz 
    var o = {
        "M+": this.getMonth() + 1, //月份 
        "d+": this.getDate(), //日 
        "h+": this.getHours(), //小时 
        "m+": this.getMinutes(), //分 
        "s+": this.getSeconds(), //秒 
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
        "S": this.getMilliseconds() //毫秒 
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}

//日期加減  timeU︰類型  byMany︰間隔(正負數)  dateObj︰日期型態
/*

    //當下日期與時間
    var now = new Date();
    var years = now.getYear();        //年
    var months = now.getMonth()+1;    //月
    var days = now.getDate();         //日
    var hours = now.getHours();       //時
    var minutes = now.getMinutes();   //分

    //90天後日期
    NextNow = DateAdd("d",90,now);
    years = NextNow.getYear();
    months = NextNow.getMonth()+1;
    days = NextNow.getDate();
    hours = NextNow.getHours();

    //3個月後日期
    ThreeMonth = DateAdd("m",3,now);
*/
Date.prototype.DateAdd = function (timeU, byMany) {
    var dateObj = this;
    var millisecond = 1;
    var second = millisecond * 1000;
    var minute = second * 60;
    var hour = minute * 60;
    var day = hour * 24;
    var year = day * 365;

    var newDate;
    var dVal = dateObj.valueOf();
    var date = new Date(dateObj); // For months
    switch (timeU) {
        case "ms":
            newDate = new Date(dVal + millisecond * byMany);
            break;
        case "s":
            newDate = new Date(dVal + second * byMany);
            break; //秒
        case "mi":
            newDate = new Date(dVal + minute * byMany);
            break; //分
        case "h":
            newDate = new Date(dVal + hour * byMany);
            break; //時
        case "m":
            newDate = new Date(date.setMonth(date.getMonth() + byMany));
            break; //月
        case "d":
            newDate = new Date(dVal + day * byMany);
            break; //日
        case "y":
            newDate = new Date(dVal + year * byMany);
            break; //年
    }
    return newDate;
}
var $f = function (options) {
    return new $f.prototype.init(options);
}
$f.prototype = {
    info: {}, //表單基本資訊
    init: function (options) {
        //初始化物件======================
        $.extend(this, options);

        this.api.event.parent = this.api;
        this.data.parent = this;
        this.detail.parent = this;
        this.event.parent = this;
        this.verify.parent = this;
        this.verify.checkFunction.parent = this.verify;
        this.view.parent = this;
        this.view.event.parent = this.view;
        
        /*
        //over write Jquery 的 .val, 使其預設觸發 change event.
        var originalValFn = jQuery.fn.val;
        jQuery.fn.val = function() {
            this.trigger('change');
            originalValFn.apply( this, arguments );
        }
        */

        //載入FM7表單後送出Function
        if (typeof Applicant_Check != 'undefined') this.event.fm7SendFunction = Applicant_Check;
        if (typeof Approve_Check != 'undefined') this.event.fm7SendFunction = Approve_Check;

        //載入表單基本資訊
        this.info.requisitionID = $('[name=RequisitionID]').val();
        this.info.applicantID = $('[name=ApplicantID]').val();
        this.info.applicantDept = $('[name=ApplicantDept]').val();
        this.info.fillerID = $('[name=FillerID]').val();
        this.info.approverID = $('[name=ApproverID]').val();
        this.info.approverDept = $('[name=ApproverDept]').val();
        this.info.processID = $('[name=ProcessID]').val();
        this.info.identify = $('[name=Identify]').val();

        if (typeof ApproveSBTN == 'undefined') this.info.processID = 'applicant';
        if (location.pathname.indexOf('Content') >= 0 ||
            location.pathname.indexOf('FM7_BPMPlus_GridDetail') >= 0) this.info.processID = 'Content';

        this.info.isApplicant = (this.info.processID == 'applicant');

        return this;
    },
    event: {
        fm7SendFunction: function () {}, //FM7表單後送出Function
        handlerSend: function (func) { //表單送出Function
            var obj = this;
            var pluginFunction = function () {
                var send = function (actionInfo) {
                    if (typeof func != 'undefined')
                        if (!func(actionInfo)) return false;

                    return obj.parent.event.fm7SendFunction();
                }
                var actionInfo = {
                    action: null
                };
                if ($('[name=Result]').val() == 1) actionInfo.action = 'agree';
                if ($('[name=Result]').val() == 2) actionInfo.action = 'reject';
                if ($('[name=DraftFlag]').val() == 1) actionInfo.action = 'draft';
                if ($('[name=Result]').length == 0 && $('[name=DraftFlag]').val() == 0) actionInfo.action = 'submit';

                return send(actionInfo);
            };
            if (typeof Applicant_Check != 'undefined') Applicant_Check = pluginFunction;
            if (typeof Approve_Check != 'undefined') Approve_Check = pluginFunction;

            return this;
        }
    }
}

$f.prototype.init.prototype = $f.prototype;
$f.prototype.view = {
    temp: {
        currentElement: null
    },
    event: {
        getViewTextEventCtrlFunction: function (text, value, element) {
            var ctrlValue = this.getViewTextEventFunction(text, value, element);
            if (typeof ctrlValue != 'undefined') value = ctrlValue;
            return value;
        },
        getViewTextEventFunction: function (text, value, element) {
            return value;
        },
        handlerGetViewText: function (func) {
            this.getViewTextEventFunction = func;
            return this.parent;
        },
    },
    bind: function (pIsSetValue) {
        if (typeof pIsSetValue == 'undefined') pIsSetValue = true;

        var obj = this;
        var viewElements = $('[sf-view]:not([sf-view-sort])').toArray();
        var viewSortElements = {};

        $('[sf-view-sort]').each(function (key, item) {
            viewSortElements[$(this).attr("sf-view-sort")] = this;
        });
        $.each(viewSortElements, function (key, item) {
            viewElements.push(item);
        });

        $(viewElements).each(function () {
            obj.temp.currentElement = this;

            var view = $(this).attr('sf-view');
            var viewClass = obj.getViewText($(this).attr('sf-view-class'));
            var isSetValue = $(this).attr('sf-view-isSetValue');
            var fillValue = obj.getViewText($(this).attr('sf-view-fillValue'));
            var fillMessage = obj.getViewText($(this).attr('sf-view-fillMessage'));
            var viewObj;

            if ((pIsSetValue && obj.parent.info.isApplicant && fillValue != null) || (isSetValue == 'always' && fillValue != null))
                $(this).val(fillValue);

            if (!isUseProcess(obj.parent.info.processID, $(this).attr('sf-view-process'))) return;

            switch (view) {
                case 'text':
                    var message;
                    if ($(this).is('select')) {
                        message = $(this).find('option:selected').text();
                    } else if ($(this).is('[type=checkbox],[type=radio]')) {
                        /*
                        var nobind = $(this).attr('sf-view-nobind');
                        var checkboxName = $(this).attr('name');

                        if (nobind == 1) return;

                        var textArry = [];
                        $('[name=checkboxName]').each(function (i, item) {
                            if (i > 0) $(this).attr('sf-view-nobind', 1);
                            if ($(this).is('checked')) {
                                var text = $(this).next('font').text();
                                textArry.push(text);
                            }
                        });
                        message = textArry.join(',');
                        */
                        message = undefined;
                    } else if ($(this).is('font')) {
                        message = $(this).text();
                    } else {
                        message = $(this).val();
                    }

                    if (isUseProcess(obj.parent.info.processID, $(this).attr('sf-view-fillMessageProcess')) &&
                        fillMessage != null)
                        message = fillMessage;
                    viewObj = obj.textMode(this, message);
                    break;
                case 'currency':
                    var num = $(this).val();
                    var parts = num.toString().split('.');
                    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                    viewObj = obj.textMode(this, parts.join('.'));
                    break;
                case 'multipleSelect':
                    var split = obj.getViewText($(this).attr('sf-view-split'));
                    viewObj = obj.multipleSelectMode(this, split)
                    break;
                case 'multipleText':
                    var split = obj.getViewText($(this).attr('sf-view-split'));
                    viewObj = obj.multipleTextMode(this, split)
                    break;
                case 'hide':
                    viewObj = $(this).hide();
                    break;
                case 'show':
                    viewObj = $(this).show();
                    break;
                case 'disabled':
                    viewObj = $(this).prop('disabled', true);
                    break;
            }
            if (viewClass != null) $(viewObj).addClass(viewClass);
        });
    },
    getViewText: function (viewText) {
        var obj = this;
        if (typeof viewText == 'undefined') return null;
        var value = viewText;

        switch (viewText) {
            case '{today}':
                value = (new Date()).Format('yyyy/MM/dd');
                value = obj.event.getViewTextEventCtrlFunction(viewText, value, obj.temp.currentElement);
                break;
            default:
                var textArry = [];
                $.each(viewText.split(/(\[.*?\])/), function (i, text) {
                    var itemValue;
                    if (text.indexOf('[') >= 0) {
                        itemValue = obj.event.getViewTextEventCtrlFunction(text, $(text).val(), obj.temp.currentElement);
                    } else {
                        itemValue = obj.event.getViewTextEventCtrlFunction(text, text, obj.temp.currentElement);
                    }
                    textArry.push(itemValue);
                });
                value = textArry.join('');
        }
        return value;
    },
    /**
     * 將HTML物件設為不可編修狀態
     * @param {o} 要設為不可編修狀態的HTML物件
     */
    textMode: function (o, text) {
        var name = $(o).attr('name') + guid();
        var sfViewText = $(o).attr('sf-view-text');

        $(o).attr('sf-view-text', name);
        $('font[sf-view-text="f_' + sfViewText + '"]').remove();

        if (typeof text == 'undefined') {
            if ($(o).is("select")) {
                text = $(o).find('option:selected').text();
            } else if ($(o).is('font')) {
                text = $(o).text();
            } else if ($(o).is('[type=checkbox],[type=radio]')) {
                $(o).prop('disabled', true);
                return;
            } else {
                text = $(o).val();
            }
        }
        var font = $('<font sf-view-text="f_' + name + '">' + text.toString().replace(/\n/g, "<br/>").trim() + '</font>');
        $(o).last().hide().after(font);
        return font;
    },
    /**
     * 回復textMode的不可編修狀態
     * @param {o} 要回復狀態的HTML物件
     */
    recoverTextMode: function (o) {
        var name = $(o).attr('name');
        $(o).nextAll('[sf-view-text="' + name + ']').remove();
        $(o).attr('disabled', false).show();
    },
    multipleSelectMode: function (o, split) {
        var valueArry = $(o).val().split((split == null ? ',' : split));
        var name = $(o).attr('name') + guid();
        var sfViewID = $(o).attr('sf-view-multipleSelect');
        var multipleSelect = $('<select multiple="multiple"></select>').attr('sf-view-multipleSelect', name);

        $(o).attr('sf-view-multipleSelect', name);
        $('select[sf-view-multipleSelect="' + sfViewID + '"]').remove();

        $.each(valueArry, function (i, value) {
            var option =
                $('<option>')
                .text(value.trim())
                .val(value.trim());
            $(multipleSelect).append(option);
        });

        $(multipleSelect).attr("size", valueArry.length);
        $(o).last()
            .hide()
            .after(multipleSelect);

        if ($(o).val() == '') $(multipleSelect).hide();

        return multipleSelect;
    },
    multipleTextMode: function (o, split) {
        var valueArry = $(o).val().split((split == null ? ',' : split));
        var name = $(o).attr('name') + guid();
        var sfViewID = $(o).attr('sf-view-multipleText');
        var multipleText = $('<div></div>').attr('sf-view-multipleText', name);

        $(o).attr('sf-view-multipleText', name);
        $('div[sf-view-multipleText="' + sfViewID + '"]').remove();

        $.each(valueArry, function (i, value) {
            var item = $('<div></div>').text(value.trim());
            $(multipleText).append(item);
        });

        $(o).last()
            .hide()
            .after(multipleText);

        return multipleText;
    }
};
$f.prototype.detail = {   //明細模組
    event: {
        AddEventFunction: function(detailId, detailItem){},   //新增明細後Function
        handlerAdd: function(func){this.AddEventFunction = func;},   //新增明細後Function
    },
    template:{},
    setTemplate: function(id, selector){
        $(selector)
            .attr('detailId', id)
            .attr('detailType', 'template')
            .hide();
        return id;
    },
    add: function(id){ //新增明細
        var item = 
        $('[detailId="'+id+'"][detailtype=template]')
            .clone()
            .attr('detailType', 'item')
            .attr('detailItemId', id)
            .show();

        $('[detailId="'+id+'"][detailtype=template]').parent().append(item);
        
        this.event.AddEventFunction(id, item);
        return item;
    },
    delete: function(itemChild){ //刪除明細
        var id = this.getId(itemChild);
        $('[detailItemId='+id+']').remove();
    },
    getId: function(itemChild){ //取得明細ID
        return this.getItem(itemChild).attr('detailItemId');
    },
    getItem: function(itemChild){ //取得明細
        return $(itemChild).parents('[detailtype=item]:first');
    }
};
$f.prototype.api = {
    exec: function (model, params) {
        if (typeof params == 'undefined') params = {};

        var result;

        $.ajax({
            async: false,
            type: "POST",
            url: 'NewtypeAPI.aspx',
            dataType: 'json',
            data: {
                model: model,
                params: JSON.stringify(params)
            },
            success: function (data) {
                result = data;
            }
        });
        return result;
    },
    bind: function () {
        var obj = this;
        $('[sf-api]').each(function () {
            obj.render(this);
        });
        return this; //sdfsfh
    },
    render: function (element) {
        var obj = this;
        var name = $(element).attr('name');
        var view = $(element).attr('sf-api');
        var model = $(element).attr('sf-api-model');
        var changeSelector = $(element).attr('sf-api-change-selector');
        var apiInfo = {
            model: model,
            params: {}
        };

        this.event.inputParamsFunction(apiInfo, element);
        var dataArry = this.exec(model, apiInfo.params);

        if (typeof changeSelector != 'undefined')
            $(changeSelector)
            .unbind("change.render" + name)
            .bind("change.render" + name, function () {
                obj.render(element);
            });

        switch (view) {
            case 'option':
                var text = $(element).attr('sf-api-text');
                var value = $(element).attr('sf-api-value');
                var emptyOption = $(element).attr('sf-api-emptyOption');

                $(element).find('option').remove();

                if (emptyOption == 'true') $(element).append($('<option>'));

                $(dataArry).each(function (i, data) {
                    //console.log(data, text, value);
                    var option =
                        $('<option>')
                        .text(eval(text))
                        .val(eval(value).trim())
                        .attr('data', JSON.stringify(data));
                    $(element).append(option);
                });
                break;
        }
        $(element).change();
        this.event.bindCompletedFunction(apiInfo, element);
        return element;
    },
    event: {
        inputParamsFunction: function (apiInfo) {},
        handlerInputParams: function (func) {
            this.inputParamsFunction = func;
            return this.parent;
        },
        bindCompletedFunction: function (apiInfo) {},
        handlerBindCompleted: function (func) {
            this.bindCompletedFunction = func;
            return this.parent;
        },
    },
};
$f.prototype.dialog = {
    bind: function (options) {
        var isIE = function () {
            return ("ActiveXObject" in window);
        }

        var defaults = {
            title: '請選擇', //標題-
            src: '', //網址-
            width: 800, //寬度-
            height: 600, //高度-
            multiple: 0, //0:單選,1:多選 -
            load: function (o) {},
            closeButtonEvent: function (o) {
                $('input:eq(0)').focus();
                $('input:eq(1)').focus();
            },
            buttons: //按鈕- 
                [
                    /*{
                    	text: 'OK',
                    	callback: function(o){
                    		alert($(o).contents().find('[f=y]').val());
                    	}
                    }*/
                ],
            objevent: //Dialog內物件的事件 
                [
                    /*{
                    	tag: 'iframe',
                    	event: 'load',
                    	callback: function(o){
                    		$(o).contents().find('#ifmbkSearch').contents().delegate('a', 'click', function(){
                    			alert('User clicked on foo.');
                    		});
                    	}
                    }*/
                ]
        };
        var options = $.extend(defaults, options);
        var objDialog = {
            dialog: null,
            frame: null,
            content: null,
            open: null
        };
        var buttons = [];
        var no = guid();
        var divDialog = '<div name="divDialog_' + no + '" style="display:none;overflow:hidden;padding:4px;">' +
            '	<div><iframe name="frameDialog_' + no + '" frameborder="0" ' + (options.multiple == 0 ? '' : 'style="float: left; width: 70%;"') + ' width="100%" height="' + (options.buttons.length > 0 ? options.height - 125 : options.height - 50) + '"></iframe></div>' +
            '   ' + (options.multiple == 0 ? '' : '	<div><select name="frameDialogSelect_' + no + '" multiple="multiple" style="font-size: 12px;width: 30%; height: 100%;"></div>') +
            '</div>';

        if (isIE()) options.width += 15;

        objDialog.dialog = $(divDialog).dialog({
            position: {
                my: "center bottom",
                at: "center top",
                of: window
            },
            title: options.title,
            autoOpen: false,
            height: options.height + 4,
            width: options.width,
            modal: true,
            close: function () {
                $('[name=frameDialog_' + no + ']').unbind('load').remove();
                $('[name=divDialog_' + no + ']').unbind('load').remove();
            },
            create: function () {
                $(this)
                    .closest('div.ui-dialog')
                    .find('.ui-dialog-titlebar-close')
                    .click(function (e) {
                        options.closeButtonEvent(objDialog);
                    });
            }
        });

        objDialog.getMultipleSelect = function () {
            return $('[name=frameDialogSelect_' + no + ']');
        }
        objDialog.open = function () {
            objDialog.dialog.dialog('open');
        }
        objDialog.close = function () {
            objDialog.dialog.dialog('close');
        }
        objDialog.frame = $('[name=frameDialog_' + no + ']')[0];

        $(objDialog.frame).load(function () {
            //Add Button
            buttons = [];
            $(options.buttons).each(function (i, o) {
                buttons.push({
                    text: o.text,
                    click: function () {
                        o.callback(objDialog)
                    }
                });
            });
            objDialog.dialog.dialog({
                buttons: buttons
            });

            objDialog.content = $('[name=frameDialog_' + no + ']').contents();
            objDialog.contentWindow = $('[name=frameDialog_' + no + ']')[0].contentWindow ? $('[name=frameDialog_' + no + ']')[0].contentWindow : $('[name=frameDialog_' + no + ']')[0].contentDocument.defaultView;
            options.load(objDialog);

            var frameDialog = this;

            /*
            $(frameDialog).css('height',0).css('height',$(frameDialog).contents().find('body')[0].scrollHeight);
            */

            //Add Window Object Event
            $(options.objevent).each(function (i, o) {
                if (o.event == 'load') {
                    var eventSet = 0;
                    $(frameDialog).contents().find(o.tag).bind(o.event, function () {
                        eventSet = 1;
                        o.callback(objDialog, this);
                    });
                    if ($(frameDialog).contents().find(o.tag).contents().find('body').length > 0 && eventSet == 0) {
                        o.callback(objDialog, this);
                    }
                }
                if (o.event == 'blur') {
                    $(frameDialog).contents().find(o.tag).bind(o.event, function () {
                        o.callback(objDialog, this);
                    });
                } else {
                    $(frameDialog).contents().delegate(o.tag, o.event, function () {
                        o.callback(objDialog, this);
                    });
                }
            });
        }).attr('src', options.src + (options.src.toString().indexOf('?') < 0 ? '?' : '&') + 'r=' + Math.random());
        return objDialog;
    },
    selector: function (options) {
        var defaults = {
            self: null,
            selectorID: '',
            filterSelectorID: '',
            accountID: '',
            defaultFilterString: '',
            multiple: 0, //0:單選,1:多選 -
            multipleFormate: '"data[fieldName]"',
            dataSource: 'db', //db, url, data arry -
            secondDataSource: null, //db, url, data arry, function(FilterStr){} -
            url: '',
            title: '請選擇',
            width: 800,
            height: 600,
            callback: function (selector, data) {}
        };
        var options = $.extend(defaults, options);

        var buttons = [];

        if (options.multiple == 1)
            buttons.push({
                text: 'Delete',
                callback: function (selector) {
                    selector.contentWindow.deleteOption();
                }
            });

        buttons.push({
            text: 'Ok',
            callback: function (selector) {
                var data = selector.contentWindow.getData();
                options.callback(selector, data);
            }
        });

        buttons.push({
            text: 'Cancel',
            callback: function (selector) {
                selector.close();
            }
        });
        var dialog = this.bind({
            load: function (selector) {
                selector.contentWindow.init(options, selector);
                selector.contentWindow.loadData(options.defaultFilterString, '');
            },
            height: options.height,
            width: options.width,
            title: options.title,
            multiple: options.multiple,
            src: 'Newtype_SFSelector.aspx?SelectorID=' + options.selectorID + '&FilterSelectorID=' + options.filterSelectorID + '&AccountID=' + options.accountID + '&Multiple=' + options.multiple + '&url=' + encodeURIComponent(options.url) + '&r=' + Math.random(),
            buttons: buttons
        });
        dialog.open();
    }
};
/*
	小數點檢查
	function CheckNumberValue(obj){
		var value = $(obj).val().replace(/[^\d.]/g,'');
		value = (value == '' ? 0 : Math.round(value*100)/100);
		if(value > 999.99) value = 999.99;
		$(obj).val(value);
	}
*/
$f.prototype.verify = {
    temp: {
        requiredElement: {}
    },
    message: {
        required: '{field} 不可空白'
    },
    checkFunction: {
        base: function (verify, elements) {
            var obj = this;
            var status = true;
            $(elements).each(function (i, element) {
                if (obj[verify](element)) {
                    status = false;
                    return;
                }
            });
            return status;
        },
        required: function (element) {
            var name = $(element).attr('name');

            if(typeof this.parent.temp.requiredElement[name] != 'undefined') return false;

            this.parent.temp.requiredElement[name] = element;

            if ($(element).is('[type=checkbox],[type=radio]')) {
                return $('[name=' + name + ']:checked').length == 0;
            } else {
                return $(element).val() == '';
            }
        }
    },
    event: {
        getFieldNameEventFunction: function (element, fieldName) {
            return fieldName;
        },
        handlerGetFieldName: function (func) {
            this.getFieldNameEventFunction = func;
            return this.parent;
        },
    },
    bind: function () {
        var obj = this;
        $('[sf-verify]').each(function () {
            var verify = $(this).attr('sf-verify');

            switch (verify) {
                case 'required':
                    break;
            }
        });
    },
    check: function (isAlert) {
        this.temp.requiredElement = {};

        if (typeof isAlert == 'undefined') isAlert = true;

        var obj = this;
        var verifyMessageArry = [];
        $('[sf-verify]').each(function () {
            if (!isUseProcess(obj.parent.info.processID, $(this).attr('sf-verify-process'))) return;

            var elements = [this];
            var verify = $(this).attr('sf-verify');
            var fieldName = $(this).attr('sf-verify-fieldName');
            var group = $(this).attr('sf-verify-group');

            if (typeof group != 'undefined') {
                var groupElements = $(group);
                if (groupElements.length > 0) elements = elements.concat(groupElements);
            }

            if (!obj.checkFunction.base(verify, elements))
                verifyMessageArry.push(obj.message[verify].replace('{field}', typeof fieldName == 'undefined' ? obj.getFieldName(this) : fieldName));
        });
        if (isAlert && verifyMessageArry.length > 0) alert(verifyMessageArry.join('\n'));

        return {
            verifyMessageArry: verifyMessageArry,
            status: verifyMessageArry.length == 0
        }
    },
    getFieldName: function (element) {
        var fieldName = $(element).attr('sf-verify-fieldName');

        if (typeof fieldName == 'undefined') fieldName = $(element).parents('td:first').prev('th').text().replace('*', '').trim();
        return this.event.getFieldNameEventFunction(element, fieldName);
    },
};
$f.prototype.data = {
    exec: function () {
        var obj = this;
        var detailData = {};
        var dataMessageArry = [];
        var isSuccessful = true;

        $('[sf-data]:not([detailtype="template"])').each(function (i, item) {
            if (!isUseProcess(obj.parent.info.processID, $(this).attr('sf-data-process'))) return;

            var isTempDetail = $(this).parents('[detailtype="template"]').length > 0;
            if (isTempDetail) return;

            var table = $(this).attr('sf-data');

            if (typeof detailData[table] == 'undefined') {
                detailData[table] = {
                    api: '',
                    itemList: []
                }
            }

            var item = {};
            var dataSettedList = {};
            $(this).find('[sf-data-field]').each(function (j, field) {
                if (!isUseProcess(obj.parent.info.processID, $(this).attr('sf-data-fieldProcess'))) return;

                var name = $(this).attr('name');
                if (dataSettedList[name] == true) return;
                
                var fieldName = $(field).attr('sf-data-field');

                if ($(this).is('[type=checkbox],[type=radio]')) {
                    var valueList = [];
                    $('[name="'+name+'"]:checked').each(function(i, item){
                        valueList.push($(item).val());
                    });
                    dataSettedList[name] = true;
                    item[fieldName] = valueList.join(',');
                }else{
                    item[fieldName] = $(field).val();
                }
            });

            if (Object.keys(item).length > 0) {
                item.RequisitionID = obj.parent.info.requisitionID;
                detailData[table].itemList.push(item);
            }

            switch (table) {
                case 'M':
                    detailData[table].api = 'NewtypeAPI_dataUpdate.aspx';
                    break;
                default:
                    detailData[table].api = 'NewtypeAPI_dataInsert.aspx';
                    break;
            }
        });

        $.each(detailData, function (table, data) {
            if (data.itemList.length > 0)
                $.ajax({
                    type: "POST",
                    url: data.api,
                    async: false,
                    dataType: "json",
                    data: {
                        requisitionID: obj.parent.info.requisitionID,
                        identify: obj.parent.info.identify,
                        table: table,
                        itemList: JSON.stringify(data.itemList)
                    },
                    success: function (result) {
                        if (result.status != 1) {
                            alert(result.message);
                            isSuccessful = false;
                            dataMessageArry.push({
                                table: table,
                                message: result.message
                            });
                        }
                    }
                });
        });

        return {
            dataMessageArry: dataMessageArry,
            status: isSuccessful
        }
    },
    getFormData: function (table) {
        var obj = this;
        data = [];
        $.ajax({
            type: "POST",
            url: "NewtypeAPI_dataSelect.aspx",
            async: false,
            dataType: "json",
            data: {
                requisitionID: obj.parent.info.requisitionID,
                identify: obj.parent.info.identify,
                table: table
            },
            success: function (result) {
                data = result;
            }
        });

        return (table == 'M' ? (data.length > 0 ? data[0] : {}) : data);
    }
};