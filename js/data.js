$f.prototype.data = {
    exec: function () {
        var obj = this;
        var detailData = {};
        var dataMessageArry = [];
        var isSuccessful = true;

        $('[sf-data]:not([detailtype="template"])').each(function (i, item) {
            if (!isUseProcess(obj.parent.info.processID, $(this).attr('sf-data-process'))) return;

            var isTempDetail = $(this).parents('[detailtype="template"]').length > 0;
            if (isTempDetail) return;

            var table = $(this).attr('sf-data');

            if (typeof detailData[table] == 'undefined') {
                detailData[table] = {
                    api: '',
                    itemList: []
                }
            }

            var item = {};
            var dataSettedList = {};
            $(this).find('[sf-data-field]').each(function (j, field) {
                if (!isUseProcess(obj.parent.info.processID, $(this).attr('sf-data-fieldProcess'))) return;

                var name = $(this).attr('name');
                if (dataSettedList[name] == true) return;
                
                var fieldName = $(field).attr('sf-data-field');

                if ($(this).is('[type=checkbox],[type=radio]')) {
                    var valueList = [];
                    $('[name="'+name+'"]:checked').each(function(i, item){
                        valueList.push($(item).val());
                    });
                    dataSettedList[name] = true;
                    item[fieldName] = valueList.join(',');
                }else{
                    item[fieldName] = $(field).val();
                }
            });

            if (Object.keys(item).length > 0) {
                item.RequisitionID = obj.parent.info.requisitionID;
                detailData[table].itemList.push(item);
            }

            switch (table) {
                case 'M':
                    detailData[table].api = 'NewtypeAPI_dataUpdate.aspx';
                    break;
                default:
                    detailData[table].api = 'NewtypeAPI_dataInsert.aspx';
                    break;
            }
        });

        $.each(detailData, function (table, data) {
            if (data.itemList.length > 0)
                $.ajax({
                    type: "POST",
                    url: data.api,
                    async: false,
                    dataType: "json",
                    data: {
                        requisitionID: obj.parent.info.requisitionID,
                        identify: obj.parent.info.identify,
                        table: table,
                        itemList: JSON.stringify(data.itemList)
                    },
                    success: function (result) {
                        if (result.status != 1) {
                            alert(result.message);
                            isSuccessful = false;
                            dataMessageArry.push({
                                table: table,
                                message: result.message
                            });
                        }
                    }
                });
        });

        return {
            dataMessageArry: dataMessageArry,
            status: isSuccessful
        }
    },
    getFormData: function (table) {
        var obj = this;
        data = [];
        $.ajax({
            type: "POST",
            url: "NewtypeAPI_dataSelect.aspx",
            async: false,
            dataType: "json",
            data: {
                requisitionID: obj.parent.info.requisitionID,
                identify: obj.parent.info.identify,
                table: table
            },
            success: function (result) {
                data = result;
            }
        });

        return (table == 'M' ? (data.length > 0 ? data[0] : {}) : data);
    }
};