// (new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423 
// (new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18 
Date.prototype.Format = function (fmt) { //author: meizz 
    var o = {
        "M+": this.getMonth() + 1, //月份 
        "d+": this.getDate(), //日 
        "h+": this.getHours(), //小时 
        "m+": this.getMinutes(), //分 
        "s+": this.getSeconds(), //秒 
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
        "S": this.getMilliseconds() //毫秒 
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}

//日期加減  timeU︰類型  byMany︰間隔(正負數)  dateObj︰日期型態
/*

    //當下日期與時間
    var now = new Date();
    var years = now.getYear();        //年
    var months = now.getMonth()+1;    //月
    var days = now.getDate();         //日
    var hours = now.getHours();       //時
    var minutes = now.getMinutes();   //分

    //90天後日期
    NextNow = DateAdd("d",90,now);
    years = NextNow.getYear();
    months = NextNow.getMonth()+1;
    days = NextNow.getDate();
    hours = NextNow.getHours();

    //3個月後日期
    ThreeMonth = DateAdd("m",3,now);
*/
Date.prototype.DateAdd = function (timeU, byMany) {
    var dateObj = this;
    var millisecond = 1;
    var second = millisecond * 1000;
    var minute = second * 60;
    var hour = minute * 60;
    var day = hour * 24;
    var year = day * 365;

    var newDate;
    var dVal = dateObj.valueOf();
    var date = new Date(dateObj); // For months
    switch (timeU) {
        case "ms":
            newDate = new Date(dVal + millisecond * byMany);
            break;
        case "s":
            newDate = new Date(dVal + second * byMany);
            break; //秒
        case "mi":
            newDate = new Date(dVal + minute * byMany);
            break; //分
        case "h":
            newDate = new Date(dVal + hour * byMany);
            break; //時
        case "m":
            newDate = new Date(date.setMonth(date.getMonth() + byMany));
            break; //月
        case "d":
            newDate = new Date(dVal + day * byMany);
            break; //日
        case "y":
            newDate = new Date(dVal + year * byMany);
            break; //年
    }
    return newDate;
}