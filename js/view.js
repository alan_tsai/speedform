$f.prototype.view = {
    temp: {
        currentElement: null
    },
    event: {
        getViewTextEventCtrlFunction: function (text, value, element) {
            var ctrlValue = this.getViewTextEventFunction(text, value, element);
            if (typeof ctrlValue != 'undefined') value = ctrlValue;
            return value;
        },
        getViewTextEventFunction: function (text, value, element) {
            return value;
        },
        handlerGetViewText: function (func) {
            this.getViewTextEventFunction = func;
            return this.parent;
        },
    },
    bind: function (pIsSetValue) {
        if (typeof pIsSetValue == 'undefined') pIsSetValue = true;

        var obj = this;
        var viewElements = $('[sf-view]:not([sf-view-sort])').toArray();
        var viewSortElements = {};

        $('[sf-view-sort]').each(function (key, item) {
            viewSortElements[$(this).attr("sf-view-sort")] = this;
        });
        $.each(viewSortElements, function (key, item) {
            viewElements.push(item);
        });

        $(viewElements).each(function () {
            obj.temp.currentElement = this;

            var view = $(this).attr('sf-view');
            var viewClass = obj.getViewText($(this).attr('sf-view-class'));
            var isSetValue = $(this).attr('sf-view-isSetValue');
            var fillValue = obj.getViewText($(this).attr('sf-view-fillValue'));
            var fillMessage = obj.getViewText($(this).attr('sf-view-fillMessage'));
            var viewObj;

            if ((pIsSetValue && obj.parent.info.isApplicant && fillValue != null) || (isSetValue == 'always' && fillValue != null))
                $(this).val(fillValue);

            if (!isUseProcess(obj.parent.info.processID, $(this).attr('sf-view-process'))) return;

            switch (view) {
                case 'text':
                    var message;
                    if ($(this).is('select')) {
                        message = $(this).find('option:selected').text();
                    } else if ($(this).is('[type=checkbox],[type=radio]')) {
                        /*
                        var nobind = $(this).attr('sf-view-nobind');
                        var checkboxName = $(this).attr('name');

                        if (nobind == 1) return;

                        var textArry = [];
                        $('[name=checkboxName]').each(function (i, item) {
                            if (i > 0) $(this).attr('sf-view-nobind', 1);
                            if ($(this).is('checked')) {
                                var text = $(this).next('font').text();
                                textArry.push(text);
                            }
                        });
                        message = textArry.join(',');
                        */
                        message = undefined;
                    } else if ($(this).is('font')) {
                        message = $(this).text();
                    } else {
                        message = $(this).val();
                    }

                    if (isUseProcess(obj.parent.info.processID, $(this).attr('sf-view-fillMessageProcess')) &&
                        fillMessage != null)
                        message = fillMessage;
                    viewObj = obj.textMode(this, message);
                    break;
                case 'currency':
                    var num = $(this).val();
                    var parts = num.toString().split('.');
                    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                    viewObj = obj.textMode(this, parts.join('.'));
                    break;
                case 'multipleSelect':
                    var split = obj.getViewText($(this).attr('sf-view-split'));
                    viewObj = obj.multipleSelectMode(this, split)
                    break;
                case 'multipleText':
                    var split = obj.getViewText($(this).attr('sf-view-split'));
                    viewObj = obj.multipleTextMode(this, split)
                    break;
                case 'hide':
                    viewObj = $(this).hide();
                    break;
                case 'show':
                    viewObj = $(this).show();
                    break;
                case 'disabled':
                    viewObj = $(this).prop('disabled', true);
                    break;
            }
            if (viewClass != null) $(viewObj).addClass(viewClass);
        });
    },
    getViewText: function (viewText) {
        var obj = this;
        if (typeof viewText == 'undefined') return null;
        var value = viewText;

        switch (viewText) {
            case '{today}':
                value = (new Date()).Format('yyyy/MM/dd');
                value = obj.event.getViewTextEventCtrlFunction(viewText, value, obj.temp.currentElement);
                break;
            default:
                var textArry = [];
                $.each(viewText.split(/(\[.*?\])/), function (i, text) {
                    var itemValue;
                    if (text.indexOf('[') >= 0) {
                        itemValue = obj.event.getViewTextEventCtrlFunction(text, $(text).val(), obj.temp.currentElement);
                    } else {
                        itemValue = obj.event.getViewTextEventCtrlFunction(text, text, obj.temp.currentElement);
                    }
                    textArry.push(itemValue);
                });
                value = textArry.join('');
        }
        return value;
    },
    /**
     * 將HTML物件設為不可編修狀態
     * @param {o} 要設為不可編修狀態的HTML物件
     */
    textMode: function (o, text) {
        var name = $(o).attr('name') + guid();
        var sfViewText = $(o).attr('sf-view-text');

        $(o).attr('sf-view-text', name);
        $('font[sf-view-text="f_' + sfViewText + '"]').remove();

        if (typeof text == 'undefined') {
            if ($(o).is("select")) {
                text = $(o).find('option:selected').text();
            } else if ($(o).is('font')) {
                text = $(o).text();
            } else if ($(o).is('[type=checkbox],[type=radio]')) {
                $(o).prop('disabled', true);
                return;
            } else {
                text = $(o).val();
            }
        }
        var font = $('<font sf-view-text="f_' + name + '">' + text.toString().replace(/\n/g, "<br/>").trim() + '</font>');
        $(o).last().hide().after(font);
        return font;
    },
    /**
     * 回復textMode的不可編修狀態
     * @param {o} 要回復狀態的HTML物件
     */
    recoverTextMode: function (o) {
        var name = $(o).attr('name');
        $(o).nextAll('[sf-view-text="' + name + ']').remove();
        $(o).attr('disabled', false).show();
    },
    multipleSelectMode: function (o, split) {
        var valueArry = $(o).val().split((split == null ? ',' : split));
        var name = $(o).attr('name') + guid();
        var sfViewID = $(o).attr('sf-view-multipleSelect');
        var multipleSelect = $('<select multiple="multiple"></select>').attr('sf-view-multipleSelect', name);

        $(o).attr('sf-view-multipleSelect', name);
        $('select[sf-view-multipleSelect="' + sfViewID + '"]').remove();

        $.each(valueArry, function (i, value) {
            var option =
                $('<option>')
                .text(value.trim())
                .val(value.trim());
            $(multipleSelect).append(option);
        });

        $(multipleSelect).attr("size", valueArry.length);
        $(o).last()
            .hide()
            .after(multipleSelect);

        if ($(o).val() == '') $(multipleSelect).hide();

        return multipleSelect;
    },
    multipleTextMode: function (o, split) {
        var valueArry = $(o).val().split((split == null ? ',' : split));
        var name = $(o).attr('name') + guid();
        var sfViewID = $(o).attr('sf-view-multipleText');
        var multipleText = $('<div></div>').attr('sf-view-multipleText', name);

        $(o).attr('sf-view-multipleText', name);
        $('div[sf-view-multipleText="' + sfViewID + '"]').remove();

        $.each(valueArry, function (i, value) {
            var item = $('<div></div>').text(value.trim());
            $(multipleText).append(item);
        });

        $(o).last()
            .hide()
            .after(multipleText);

        return multipleText;
    }
};