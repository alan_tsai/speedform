$f.prototype.api = {
    exec: function (model, params) {
        if (typeof params == 'undefined') params = {};

        var result;

        $.ajax({
            async: false,
            type: "POST",
            url: 'NewtypeAPI.aspx',
            dataType: 'json',
            data: {
                model: model,
                params: JSON.stringify(params)
            },
            success: function (data) {
                result = data;
            }
        });
        return result;
    },
    bind: function () {
        var obj = this;
        $('[sf-api]').each(function () {
            obj.render(this);
        });
        return this; //sdfsfh
    },
    render: function (element) {
        var obj = this;
        var name = $(element).attr('name');
        var view = $(element).attr('sf-api');
        var model = $(element).attr('sf-api-model');
        var changeSelector = $(element).attr('sf-api-change-selector');
        var apiInfo = {
            model: model,
            params: {}
        };

        this.event.inputParamsFunction(apiInfo, element);
        var dataArry = this.exec(model, apiInfo.params);

        if (typeof changeSelector != 'undefined')
            $(changeSelector)
            .unbind("change.render" + name)
            .bind("change.render" + name, function () {
                obj.render(element);
            });

        switch (view) {
            case 'option':
                var text = $(element).attr('sf-api-text');
                var value = $(element).attr('sf-api-value');
                var emptyOption = $(element).attr('sf-api-emptyOption');

                $(element).find('option').remove();

                if (emptyOption == 'true') $(element).append($('<option>'));

                $(dataArry).each(function (i, data) {
                    //console.log(data, text, value);
                    var option =
                        $('<option>')
                        .text(eval(text))
                        .val(eval(value).trim())
                        .attr('data', JSON.stringify(data));
                    $(element).append(option);
                });
                break;
        }
        $(element).change();
        this.event.bindCompletedFunction(apiInfo, element);
        return element;
    },
    event: {
        inputParamsFunction: function (apiInfo) {},
        handlerInputParams: function (func) {
            this.inputParamsFunction = func;
            return this.parent;
        },
        bindCompletedFunction: function (apiInfo) {},
        handlerBindCompleted: function (func) {
            this.bindCompletedFunction = func;
            return this.parent;
        },
    },
};