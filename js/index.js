﻿var $f = function (options) {
    return new $f.prototype.init(options);
}
$f.prototype = {
    info: {}, //表單基本資訊
    init: function (options) {
        //初始化物件======================
        $.extend(this, options);

        this.api.event.parent = this.api;
        this.data.parent = this;
        this.detail.parent = this;
        this.event.parent = this;
        this.verify.parent = this;
        this.verify.checkFunction.parent = this.verify;
        this.view.parent = this;
        this.view.event.parent = this.view;
        
        /*
        //over write Jquery 的 .val, 使其預設觸發 change event.
        var originalValFn = jQuery.fn.val;
        jQuery.fn.val = function() {
            this.trigger('change');
            originalValFn.apply( this, arguments );
        }
        */

        //載入FM7表單後送出Function
        if (typeof Applicant_Check != 'undefined') this.event.fm7SendFunction = Applicant_Check;
        if (typeof Approve_Check != 'undefined') this.event.fm7SendFunction = Approve_Check;

        //載入表單基本資訊
        this.info.requisitionID = $('[name=RequisitionID]').val();
        this.info.applicantID = $('[name=ApplicantID]').val();
        this.info.applicantDept = $('[name=ApplicantDept]').val();
        this.info.fillerID = $('[name=FillerID]').val();
        this.info.approverID = $('[name=ApproverID]').val();
        this.info.approverDept = $('[name=ApproverDept]').val();
        this.info.processID = $('[name=ProcessID]').val();
        this.info.identify = $('[name=Identify]').val();

        if (typeof ApproveSBTN == 'undefined') this.info.processID = 'applicant';
        if (location.pathname.indexOf('Content') >= 0 ||
            location.pathname.indexOf('FM7_BPMPlus_GridDetail') >= 0) this.info.processID = 'Content';

        this.info.isApplicant = (this.info.processID == 'applicant');

        return this;
    },
    event: {
        fm7SendFunction: function () {}, //FM7表單後送出Function
        handlerSend: function (func) { //表單送出Function
            var obj = this;
            var pluginFunction = function () {
                var send = function (actionInfo) {
                    if (typeof func != 'undefined')
                        if (!func(actionInfo)) return false;

                    return obj.parent.event.fm7SendFunction();
                }
                var actionInfo = {
                    action: null
                };
                if ($('[name=Result]').val() == 1) actionInfo.action = 'agree';
                if ($('[name=Result]').val() == 2) actionInfo.action = 'reject';
                if ($('[name=DraftFlag]').val() == 1) actionInfo.action = 'draft';
                if ($('[name=Result]').length == 0 && $('[name=DraftFlag]').val() == 0) actionInfo.action = 'submit';

                return send(actionInfo);
            };
            if (typeof Applicant_Check != 'undefined') Applicant_Check = pluginFunction;
            if (typeof Approve_Check != 'undefined') Approve_Check = pluginFunction;

            return this;
        }
    }
}

$f.prototype.init.prototype = $f.prototype;